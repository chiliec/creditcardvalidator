//
//  CreditCardValidatorTests.swift
//  CreditCardValidatorTests
//
//  Created by Бабин Владимир on 01.01.2020.
//  Copyright © 2020 devtodev. All rights reserved.
//

import XCTest
@testable import CreditCardValidator

class CreditCardValidatorTests: XCTestCase {

    private let validator = CreditCardValidator()

    func testValidateWorks() {
        let values: [String: CreditCardError?] = [
            "52121320122917F": .notValidNumber,
            "49010AAAAAAAAAA": .notValidNumber,
            "0621094800000002": .notValidNumber,
            "11111111111": .notValidNumber,
            "111111111111": .notPassLuhnAlgorithm,
            "1111111111111111111": .notPassLuhnAlgorithm,
            "11111111111111111111": .notValidNumber,
            "4929804463622139": nil,
            "4929804463622138": .notPassLuhnAlgorithm,
            "6762765696545485": nil,
            "5212132012291762": .notPassLuhnAlgorithm,
            "6210948000000029": nil,
        ]
        values.forEach { (cardNumber, expectedError) in
            validator.checkInfo(cardNumber: cardNumber) { (cardInfo, error) in
                XCTAssertEqual(error as? CreditCardError, expectedError)
            }
        }
    }

    func testParsingAdditionalInfo() {
        let expect = expectation(description: "Waiting for network")
        validator.checkInfo(cardNumber: "4929804463622139") { (info, error) in
            guard error == nil else {
                XCTFail("Error: \(error.debugDescription)")
                return expect.fulfill()
            }
            guard let info = info else {
                XCTFail("Card info is empty")
                return expect.fulfill()
            }
            XCTAssertEqual(info.scheme, "visa")
            XCTAssertEqual(info.type, "credit")
            XCTAssertFalse(info.prepaid)
            XCTAssertEqual(info.country.countryCode, "GB")
            XCTAssertEqual(info.country.country, "United Kingdom of Great Britain and Northern Ireland")
            XCTAssertEqual(info.country.currency, "GBP")
            XCTAssertEqual(info.bank.phone, "-3509360")
            expect.fulfill()
        }
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("Test timeouted with error: \(error)")
            }
        }
    }
}
