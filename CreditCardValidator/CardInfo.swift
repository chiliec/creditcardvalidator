//
//  CardInfo.swift
//  CreditCardValidator
//
//  Created by Бабин Владимир on 01.01.2020.
//  Copyright © 2020 devtodev. All rights reserved.
//

import Foundation

struct CardInfo: Decodable {
    let scheme: String
    let type: String
    let brand: String
    let prepaid: Bool
    let country: CountryInfo
    let bank: BankInfo
}

struct BankInfo: Decodable {
    let name: String?
    let url: String?
    let phone: String?
    let city: String?
}

struct CountryInfo: Decodable {
    let country: String
    let countryCode: String
    let currency: String

    enum CodingKeys: String, CodingKey {
        case country = "name"
        case countryCode = "alpha2"
        case currency
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        country = try container.decode(String.self, forKey: .country)
        countryCode = try container.decode(String.self, forKey: .countryCode)
        currency = try container.decode(String.self, forKey: .currency)
    }
}
