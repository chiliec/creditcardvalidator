//
//  CreditCardValidator.swift
//  CreditCardValidator
//
//  Created by Бабин Владимир on 01.01.2020.
//  Copyright © 2020 devtodev. All rights reserved.
//

import Foundation

enum CreditCardError: Error {
    case notValidNumber
    case notPassLuhnAlgorithm
    case emptyData
    case invalidResponseCode
    case invalidMimeType
}

public struct CreditCardValidator {

    func checkInfo(cardNumber: String, callback: @escaping (CardInfo?, Error?) -> ()) -> Void {
        guard isValid(cardNumber: cardNumber) else {
            return callback(nil, CreditCardError.notValidNumber)
        }
        guard luhnCheck(cardNumber: cardNumber) else {
            return callback(nil, CreditCardError.notPassLuhnAlgorithm)
        }
        let url = URL(string: "https://lookup.binlist.net/\(cardNumber)")!
        var request = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad)
        request.addValue("3", forHTTPHeaderField: "Accept-Version")
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                return callback(nil, error)
            }
            guard let data = data else {
                return callback(nil, CreditCardError.emptyData)
            }
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                return callback(nil, CreditCardError.invalidResponseCode)
            }
            guard let mime = response.mimeType, mime == "application/json" else {
                return callback(nil, CreditCardError.invalidMimeType)
            }
            do {
                let decoder = JSONDecoder()
                let result = try decoder.decode(CardInfo.self, from: data)
                return callback(result, nil)
            } catch {
                return callback(nil, error)
            }
        }.resume()
    }

    private func isValid(cardNumber: String) -> Bool {
        return cardNumber.reduce(true, { return $0 && $1.isNumber })
            && !cardNumber.starts(with: "0")
            && (12...19).contains(cardNumber.count)
    }

    /**
     Simple checksum formula also known as the "mod 10" algorithm.
     See more at https://en.wikipedia.org/wiki/Luhn_algorithm
     */
    private func luhnCheck(cardNumber: String) -> Bool {
        var sum = 0
        let reversedCharacters = cardNumber.reversed().map { String($0) }
        for (idx, element) in reversedCharacters.enumerated() {
            guard let digit = Int(element) else { return false }
            switch ((idx % 2 == 1), digit) {
            case (true, 9):
                sum += 9
            case (true, 0...8):
                sum += (digit * 2) % 9
            default:
                sum += digit
            }
        }
        return sum % 10 == 0
    }
}
